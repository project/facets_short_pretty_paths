<?php

namespace Drupal\facets_short_pretty_paths\Plugin\facets_pretty_paths\coder;

use Drupal\Core\Site\Settings;
use Drupal\facets_pretty_paths\Coder\CoderPluginBase;
use Drupal\taxonomy\Entity\Term;

/**
 * Taxonomy machine name facets pretty paths coder.
 *
 * @FacetsPrettyPathsCoder(
 *   id = "taxonomy_term_machine_name_coder",
 *   label = @Translation("Taxonomy term machine name"),
 *   description = @Translation("Use term machine name (with a fallback to the term name with id if not available), e.g. /color/<strong>blue</strong>")
 * )
 */
class TaxonomyTermCoder extends CoderPluginBase {

  /**
   * TRUE if dashes should be used instead of underscores.
   *
   * @var bool
   */
  protected bool $shouldUseDashes;

  /**
   * The machine name field ID.
   *
   * @var string
   */
  protected string $fieldName;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->shouldUseDashes = (bool) Settings::get('facets_short_pretty_paths_use_dashes', FALSE);
    $this->fieldName = Settings::get('facets_short_pretty_paths_field_name', 'machine_name');
  }

  /**
   * Encode an id into an alias.
   *
   * @param string $id
   *   An entity id.
   *
   * @return string
   *   An alias.
   */
  public function encode($id) {
    $result = $id;
    if ($term = Term::load($id)) {
      $field_name = $this->fieldName;
      if ($term->hasField($field_name) && !$term->get($field_name)->isEmpty()) {
        $result = $term->get($field_name)->value;
        $result = $this->convertMachineName($result, TRUE);
      }
      // Fallback to term name with id.
      else {
        $term_name = $term->get('name')->value;
        $term_name = \Drupal::service('pathauto.alias_cleaner')
          ->cleanString($term_name);
        $result = $term_name . $this->getFallbackSeparator() . $id;
      }
    }
    return $result;
  }

  /**
   * Decodes an alias back to an id.
   *
   * @param string $alias
   *   An alias.
   *
   * @return string
   *   An id.
   */
  public function decode($alias) {
    $result = '';
    // Fallback to id if machine name is not available.
    $exploded = explode($this->getFallbackSeparator(), $alias);
    if (count($exploded) > 1) {
      $result = array_pop($exploded);
    }
    // If it does not contain a dash / id, the machine name is available.
    else {
      $termStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
      $alias = $this->convertMachineName($alias, FALSE);
      $query = $termStorage->getQuery()
        ->accessCheck()
        ->condition($this->fieldName, $alias);
      $termIds = $query->execute();
      if (!empty($termIds)) {
        $result = reset($termIds);
      }
    }
    return $result;
  }

  /**
   * Returns the separator to use between a term name and the term ID.
   *
   * @return string
   *   The separator, either ':' or '-'.
   */
  protected function getFallbackSeparator(): string {
    // If the setting is enabled, it means we want to use dashes instead of
    // underscores in the machine names. Therefore, dashes can't also be used
    // for separating the term name and ID. Switch to using a colon.
    return $this->shouldUseDashes ? ':' : '-';
  }

  /**
   * Encodes/decodes a machine name into a displayable string.
   *
   * If the use_dashes setting is enabled, it means we want to use dashes
   * instead of underscores in the machine names. This is better for Google
   * SEO.
   *
   * @param string $name
   *   The machine name.
   * @param bool $encode
   *   TRUE for encoding, FALSE for decoding.
   * @return string
   *   The processed machine name
   */
  protected function convertMachineName(string $name, bool $encode): string {
    if (!$this->shouldUseDashes) {
      return $name;
    }
    if ($encode) {
      // Encoding.
      return str_replace('_', '-', $name);
    }
    // Decoding.
    return str_replace('-', '_', $name);
  }

}
