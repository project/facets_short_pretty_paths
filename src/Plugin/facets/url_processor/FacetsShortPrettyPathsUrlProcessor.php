<?php

namespace Drupal\facets_short_pretty_paths\Plugin\facets\url_processor;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\facets\Entity\Facet;
use Drupal\facets\FacetInterface;
use Drupal\facets_pretty_paths\Plugin\facets\url_processor\FacetsPrettyPathsUrlProcessor;

/**
 * Short pretty paths URL processor.
 *
 * @FacetsUrlProcessor(
 *   id = "facets_short_pretty_paths",
 *   label = @Translation("Short pretty paths"),
 *   description = @Translation("Pretty paths uses slashes for facet aliases and dashes as separator for facet items, e.g. /brand/drupal/color/blue-white"),
 * )
 */
class FacetsShortPrettyPathsUrlProcessor extends FacetsPrettyPathsUrlProcessor implements ContainerFactoryPluginInterface {

  public const FACET_ITEMS_DELIMITER = '.';

  /**
   * {@inheritdoc}
   */
  public function buildUrls(FacetInterface $facet, array $results) {

    // No results are found for this facet, so don't try to create urls.
    if (empty($results)) {
      return [];
    }

    $initialized_coders = [];
    $initialized_facets = [];
    $filters = $this->getActiveFilters();
    $coder_plugin_manager = \Drupal::service('plugin.manager.facets_pretty_paths.coder');
    $coder_id = $facet->getThirdPartySetting('facets_pretty_paths', 'coder', 'default_coder');
    $coder = $coder_plugin_manager->createInstance($coder_id, ['facet' => $facet]);

    /** @var \Drupal\facets\Result\ResultInterface $result */
    foreach ($results as &$result) {
      $raw_value = $result->getRawValue();

      $filters_current_result = $filters;
      // If the value is active, remove the filter string from the parameters.
      if ($result->isActive()) {
        if (($key = array_search($raw_value, $filters_current_result[$result->getFacet()->id()])) !== FALSE) {
          unset($filters_current_result[$result->getFacet()->id()][$key]);
        }
        if ($result->getFacet()->getEnableParentWhenChildGetsDisabled() && $result->getFacet()->getUseHierarchy()) {
          // Enable parent id again if exists.
          $parent_ids = $result->getFacet()->getHierarchyInstance()->getParentIds($raw_value);
          if (isset($parent_ids[0]) && $parent_ids[0]) {
            $filters_current_result[$result->getFacet()->id()][] = $coder->encode($parent_ids[0]);
          }
        }
      }
      // If the value is not active, add the filter string.
      else {
        // Exclude others results if we are in the show_only_one_result mode.
        if ($result->getFacet()->getShowOnlyOneResult()) {
          $filters_current_result[$result->getFacet()->id()] = [0 => $raw_value];
        }
        else {
          $filters_current_result[$result->getFacet()->id()][] = $raw_value;
        }

        if ($result->getFacet()->getUseHierarchy()) {
          // If hierarchy is active, unset parent trail and every child when
          // building the enable-link to ensure those are not enabled anymore.
          $parent_ids = $result->getFacet()->getHierarchyInstance()->getParentIds($raw_value);
          $child_ids = $result->getFacet()->getHierarchyInstance()->getNestedChildIds($raw_value);
          $parents_and_child_ids = array_merge($parent_ids, $child_ids);
          foreach ($parents_and_child_ids as $id) {
            if (($key = array_search($id, $filters_current_result[$result->getFacet()->id()])) !== FALSE) {
              unset($filters_current_result[$result->getFacet()->id()][$key]);
            }
          }
        }
      }

      // Now we start transforming $filters_current_result array into a string
      // which we append later to the current path.
      $pretty_paths_presort_data = [];
      foreach ($filters_current_result as $facet_id => $active_values) {
        foreach ($active_values as $active_value) {
          // Ensure we only load every facet and coder once.
          if (!isset($initialized_facets[$facet_id])) {
            $facet = Facet::load($facet_id);
            $initialized_facets[$facet_id] = $facet;
            $coder_id = $facet->getThirdPartySetting('facets_pretty_paths', 'coder', 'default_coder');
            $coder = $coder_plugin_manager->createInstance($coder_id, ['facet' => $facet]);
            $initialized_coders[$facet_id] = $coder;
          }
          $encoded_value = $initialized_coders[$facet_id]->encode($active_value);
          $pretty_paths_presort_data[] = [
            'weight' => $initialized_facets[$facet_id]->getWeight(),
            'name' => $initialized_facets[$facet_id]->getName(),
            'pretty_path_alias' => "/" . $initialized_facets[$facet_id]->getUrlAlias() . "/" . $encoded_value,
          ];
        }
      }
      $pretty_paths_presort_data = $this->sortByWeightAndName($pretty_paths_presort_data);

      // Short pretty paths begin.
      $aggregated_data = [];
      foreach ($pretty_paths_presort_data as $paths_data) {
        if (!array_key_exists($paths_data['name'], $aggregated_data)) {
          $aggregated_data[$paths_data['name']] = $paths_data['pretty_path_alias'];
        }
        else {
          $tmp_alias = $aggregated_data[$paths_data['name']];
          $path_alias_parts = explode('/', $paths_data['pretty_path_alias']);
          $facet_item_alias = array_pop($path_alias_parts);
          $tmp_alias = $tmp_alias . static::FACET_ITEMS_DELIMITER . $facet_item_alias;
          $aggregated_data[$paths_data['name']] = $tmp_alias;
        }
      }
      $pretty_paths_string = implode('', $aggregated_data);
      // Short pretty paths end.

      // Remove the leading slash.
      $pretty_paths_string = ltrim($pretty_paths_string, '/');
      // Get the existing URL.
      $url = Url::fromUri('internal:' . $facet->getFacetSource()->getPath());
      // Set the facets_query to the updated facets.
      $url->setRouteParameter('facets_query', $pretty_paths_string);
      $url->setOption('attributes', ['rel' => 'nofollow']);

      // First get the current list of get parameters.
      $get_params = $this->request->query;
      // When adding/removing a filter the number of pages may have changed,
      // possibly resulting in an invalid page parameter.
      if ($get_params->has('page')) {
        $current_page = $get_params->get('page');
        $get_params->remove('page');
      }
      $url->setOption('query', $get_params->all());
      $result->setUrl($url);
      // Restore page parameter again. See https://www.drupal.org/node/2726455.
      if (isset($current_page)) {
        $get_params->set('page', $current_page);
      }
    }

    return $results;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeActiveFilters() {
    $facet_source_id = $this->configuration['facet']->getFacetSourceId();
    /** @var \Drupal\facets_short_pretty_paths\ShortPrettyPathsActiveFilters $activeFiltersService */
    $activeFiltersService = \Drupal::service('facets_short_pretty_paths.active_filters');
    $this->activeFilters = $activeFiltersService->getActiveFilters($facet_source_id);
  }

}
