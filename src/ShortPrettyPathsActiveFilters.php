<?php

namespace Drupal\facets_short_pretty_paths;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\facets_pretty_paths\Coder\CoderPluginManager;
use Drupal\facets_pretty_paths\PrettyPathsActiveFilters;
use Drupal\facets_short_pretty_paths\Plugin\facets\url_processor\FacetsShortPrettyPathsUrlProcessor;

/**
 * Determines the Short Pretty Paths active filters on a given request.
 */
class ShortPrettyPathsActiveFilters {

  /**
   * The service responsible for determining the active filters.
   *
   * @var \Drupal\facets_pretty_paths\PrettyPathsActiveFilters
   */
  protected $facetsPrettyPathsActiveFiltersService;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Coder plugin manager.
   *
   * @var \Drupal\facets_pretty_paths\Coder\CoderPluginManager
   */
  protected $coderManager;

  /**
   * Constructs an instance of PrettyPathsActiveFilters.
   *
   * @param \Drupal\facets_pretty_paths\PrettyPathsActiveFilters $activeFilters
   *   The Facets pretty paths active filters service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\facets_pretty_paths\Coder\CoderPluginManager $coderManager
   *   The Coder plugin manager.
   */
  public function __construct(PrettyPathsActiveFilters $activeFilters, EntityTypeManagerInterface $entityTypeManager, CoderPluginManager $coderManager) {
    $this->facetsPrettyPathsActiveFiltersService = $activeFilters;
    $this->entityTypeManager = $entityTypeManager;
    $this->coderManager = $coderManager;
  }

  /**
   * Returns the active filters for a given Facet source ID.
   *
   * @param string $facet_source_id
   *   The Facet Source ID.
   *
   * @return array
   *   The array of active filters.
   */
  public function getActiveFilters($facet_source_id) {
    // Do heavy lifting only once per facet source id.
    $mapping = &drupal_static('facets_pretty_paths_init', []);

    if ($mapping && isset($mapping[$facet_source_id])) {
      return $mapping[$facet_source_id];
    }

    $mapping[$facet_source_id] = [];

    // Keep a local cache of already initialised coders.
    $initialized_coders = [];

    // getFiltersFromRoute method is protected.
    $getFiltersFromRouteMethod = new \ReflectionMethod($this->facetsPrettyPathsActiveFiltersService, 'getFiltersFromRoute');
    $getFiltersFromRouteMethod->setAccessible(TRUE);
    $filters = $getFiltersFromRouteMethod->invoke($this->facetsPrettyPathsActiveFiltersService);
    if (!$filters) {
      return $mapping[$facet_source_id];
    }

    $parts = explode('/', $filters);
    if (count($parts) % 2 !== 0) {
      // Our key/value combination should always be even. If uneven, we just
      // assume that the first string is not part of the filters, and remove
      // it. This can occur when an url lives in the same path as our facet
      // source, e.g. /search/overview where /search is the facet source path.
      array_shift($parts);
    }

    // Short pretty paths begin.
    $tmp_parts = [];
    $current_alias = '';
    foreach ($parts as $index => $part) {
      if ($index % 2 == 0) {
        $current_alias = $part;
      }
      else {
        $facet_items = explode(FacetsShortPrettyPathsUrlProcessor::FACET_ITEMS_DELIMITER, $part);
        foreach ($facet_items as $facet_item) {
          if (!empty($current_alias)) {
            $tmp_parts[] = $current_alias;
            $tmp_parts[] = $facet_item;
          }
        }
      }
    }
    $parts = $tmp_parts;
    $url_alias = '';
    // Short pretty paths end.
    foreach ($parts as $index => $part) {
      if ($index % 2 == 0) {
        $url_alias = $part;
      }
      else {
        // The $url_alias comes from the previous (odd) iteration.
        // getFacetIdByUrlAlias method is protected.
        $getFacetIdByUrlAliasMethod = new \ReflectionMethod($this->facetsPrettyPathsActiveFiltersService, 'getFacetIdByUrlAlias');
        $getFacetIdByUrlAliasMethod->setAccessible(TRUE);
        $facet_id = $getFacetIdByUrlAliasMethod->invokeArgs($this->facetsPrettyPathsActiveFiltersService, [$url_alias, $facet_source_id]);
        if (!$facet_id) {
          // No valid facet url alias specified in url.
          continue;
        }
        // Only initialize facet and their coder once per facet id.
        if (!isset($initialized_coders[$facet_id])) {
          /** @var \Drupal\facets\FacetInterface $facet */
          $facet = $this->entityTypeManager->getStorage('facets_facet')->load($facet_id);
          $coder_id = $facet->getThirdPartySetting('facets_pretty_paths', 'coder', 'default_coder');
          $coder = $this->coderManager->createInstance($coder_id, ['facet' => $facet]);
          $initialized_coders[$facet_id] = $coder;
        }
        if (!isset($mapping[$facet_source_id][$facet_id])) {
          $mapping[$facet_source_id][$facet_id] = [$initialized_coders[$facet_id]->decode($part)];
        }
        else {
          $mapping[$facet_source_id][$facet_id][] = $initialized_coders[$facet_id]->decode($part);
        }
      }
    }
    return $mapping[$facet_source_id];
  }

}
