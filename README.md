Shortens Facets Pretty Paths:

- Removes the term ids from the term aliases.
If a machine name is not available yet, there is a fallback
to the default term coder, with the term id, to guarantee unicity.
- Multiple values (Facet items) are delimited by a dot,
the Facet alias is present only once.

So we have e.g.: /color/black.yellow.red
instead of /color/black-1/color/yellow-2/color/red-3

## Dependencies

- [Facets Pretty Paths](https://www.drupal.org/project/facets_pretty_paths/)
- Optional: [Taxonomy Machine Name](https://www.drupal.org/project/taxonomy_machine_name/)

## Configuration

To comply with the previous required dependency, the default
taxonomy field name used for the path resolution is the base field
definition from Taxonomy Machine Name: `machine_name`.

If you wish to use another one, it can be overridden as
`$settings['facets_short_pretty_paths_field_name'] = 'field_name';`

It's possible to show the machine names using dashes instead of underscores, which is generally better for SEO.
To enable this, enable this setting:
`$settings['facets_short_pretty_paths_use_dashes'] = TRUE;`

## Related Facet Pretty Paths issues:

A contributed solution has been favoured to a patch as

- we introduce another dependency
(https://www.drupal.org/project/facets_pretty_paths/issues/3024640)
- it required to override the active_filters service
(https://www.drupal.org/project/facets_pretty_paths/issues/2878617)

## Roadmap

- Make the facet items delimiter configurable
